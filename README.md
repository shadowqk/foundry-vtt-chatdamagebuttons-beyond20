# Foundry VTT - Chat Damage Buttons - Beyond20 Edition

This module adds damage buttons to rolls in messages from Beyond20. Follows the same format as [Chat Damage Buttons](https://gitlab.com/hooking/foundry-vtt---chat-damage-buttons) and [Chat Damage Buttons - Better Rolls Edition](https://github.com/syl3r86/chatdamagebuttons-betterrolls).

![Damage Button Preview](https://gitlab.com/Ionshard/foundry-vtt-chatdamagebuttons-beyond20/wikis/uploads/df87709b6e81c292b3b9c87e4a5b8286/ChatDamageButtons-Beyond20.png)

## Install

### Foundry Add-On Module Installation

- Go to Foundry VTT's Configuration and Setup Menu
- Select the Add-on Modules tab
- Select the Install Module button
- Use the link [https://gitlab.com/Ionshard/foundry-vtt-chatdamagebuttons-beyond20/-/jobs/artifacts/master/raw/module.json?job=build-module](https://gitlab.com/Ionshard/foundry-vtt-chatdamagebuttons-beyond20/-/jobs/artifacts/master/raw/module.json?job=build-module) as the Manifest URL
- Select Install

### Manual Install

Download the [chatdamagebuttons-beyond20.zip](https://gitlab.com/Ionshard/foundry-vtt-chatdamagebuttons-beyond20/-/jobs/artifacts/master/raw/chatdamagebuttons-beyond20.zip?job=build-module) module and extract it to Foundry VTT's `resources/app/public/modules` directory.

## License
<a rel="license" href="https://spdx.org/licenses/MIT.html"><img alt="MIT License" style="border-width:0" src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/License_icon-mit-88x31-2.svg/88px-License_icon-mit-88x31-2.svg.png" /></a> Chat Damage Buttons - Beyond20 Edition - a module for Foundry VTT - by <a xmlns:cc="http://creativecommons.org/ns#" href="https://gitlab.com/Ionshard/foundry-vtt-anvil-menu" property="cc:attributionName" rel="cc:attributionURL">Victor Ling</a> is licensed under an <a rel="license" href="https://spdx.org/licenses/MIT.html"> MIT License</a>.

This work is derived from:
- [Chat Damage Buttons - Better Rolls Edition](https://github.com/syl3r86/chatdamagebuttons-betterrolls) by [Felix Müller](https://github.com/syl3r86?tab=repositories) which is now part of [Better Rolls 5e](https://github.com/RedReign/FoundryVTT-BetterRolls5e) by [Red Reign](https://github.com/RedReign)
- [Chat Damage Buttons](https://gitlab.com/hooking/foundry-vtt---chat-damage-buttons) by [Hooking](https://gitlab.com/hooking)

This work is licensed under the Foundry Virtual Tabletop [Limited License Agreement for Module Development](https://foundryvtt.com/article/license/).


