const damageContainerStyles = {
  alignSelf: "flex-end",
  textAlign: "right",
  flexGrow: 1,
  position: "absolute",
  bottom: "1px",
  right: 0,
}

const damageButtonStyles = {
  width: "17px",
  height: "17px",
  fontSize: "9px",
  lineHeight: "1px",
  marginLeft: "1px",
  paddingLeft: "2px",
}

const i18n = (key) => {
  return game.i18n.localize(key);
}

const addDamageButtons = (element, damage) => {
  // creating the buttons and container
  const fullDamageButton = $(
    `<button data-damage="${damage}" data-modifier="1"><i class="fas fa-user-minus" title="${i18n('cdbb20.fulldamage')}"></i></button>`
  ).css(damageButtonStyles);
  const halfDamageButton = $(
    `<button data-damage="${damage}" data-modifier="0.5"><i class="fas fa-user-shield" title="${i18n('cdbb20.halfdamage')}"></i></button>`
  ).css(damageButtonStyles);
  const doubleDamageButton = $(
    `<button data-damage="${damage}" data-modifier="2"><i class="fas fa-user-injured" title="${i18n('cdbb20.doubledamage')}"></i></button>`
  ).css(damageButtonStyles);
  const fullHealingButton = $(
    `<button data-damage="${damage}" data-modifier="-1"><i class="fas fa-user-plus" title="${i18n('cdbb20.healing')}"></i></button>`
  ).css(damageButtonStyles);

  const btnContainer = $('<span class="dmgBtn-container-b20"></span>').css(damageContainerStyles);

  btnContainer.append(fullDamageButton);
  btnContainer.append(halfDamageButton);
  btnContainer.append(doubleDamageButton);
  btnContainer.append(fullHealingButton);

  // adding the buttons to the the target element
  $(element).prepend(btnContainer);
  $(element).css("min-height", "20px");
};

const applyDamage = (amount, multiplier) => {
  return Promise.all(game.user.targets.forEach(t => {
    const a = t.actor;
    console.log(`Target: ${a}`);
    return a.applyDamage(amount, multiplier);
  }));
}

const updateHandlers = html => {
  // adding click events to the buttons, this gets redone since they can break through rerendering of the card
  html.find(".dmgBtn-container-b20 button").click(async ev => {
    ev.stopPropagation();
    const button =
      ev.target.nodeName === "BUTTON" ? ev.target : ev.target.parentElement;
    const { damage } = button.dataset;
    const { modifier } = button.dataset;
    applyDamage(damage, modifier);
  });

  // logic to only show the buttons when the mouse is within the chatcard
  html.find(".dmgBtn-container-b20").hide();
  $(html).hover(
    () => {
      html.find(".dmgBtn-container-b20").show();
    },
    () => {
      html.find(".dmgBtn-container-b20").hide();
    }
  );
};

Hooks.on("renderChatMessage", (message, html, data) => {
  const chatCard = html.find(".beyond20-message");
  if (chatCard.length === 0) {
    return;
  }

  const dmgElements = chatCard
    .find("> .beyond20-roll-result")
    .not(".beyond20-roll-cells");

  dmgElements.each((_, dmgElement) => {
    let dmgValue = $(dmgElement)
      .find(".beyond20-tooltip > span:first-child")
      .text()
      .trim();

    if (dmgValue.length !== 0) {
      addDamageButtons(dmgElement, dmgValue);
    }
  });

  updateHandlers(html);
});
